<?php

namespace controllers;

use services\CountryDayReportService;
use utils\Request;
use utils\Validate;

class CountryDayReportController
{
    private readonly CountryDayReportService $countryDayReportService;
    public function __construct()
    {
        $this->countryDayReportService = new CountryDayReportService();
    }

    public function getAll(Request $request)
    {
        $data = Validate::perform(['from_date' => ['when_present', 'nullable', 'date']], $request->values());
        return $this->countryDayReportService->getPaginated(
            $request->getUrlParam('pageNo', 1),
            $request->getUrlParam('pageSize', 10),
            $request->getUrlParam('orderBy', 'id'),
            $request->getUrlParam('orderDir', 'asc'),
            $data['from_date'] ?? null
        );
    }

    public function getDirectCalculation(Request $request)
    {
        $data = Validate::perform(['from_date' => ['when_present', 'nullable', 'date']], $request->values());
        return $this->countryDayReportService->getDirectCalculation($data['from_date'] ?? null);
    }
}