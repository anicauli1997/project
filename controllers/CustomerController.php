<?php

namespace controllers;

use Cassandra\Exception\ValidationException;
use http\Exception\InvalidArgumentException;
use services\CustomerService;
use utils\Request;
use utils\Response;
use utils\Validate;

class CustomerController
{
    private readonly CustomerService $customerService;
    public function __construct()
    {
        $this->customerService = new CustomerService();
    }

    public function getAll(Request $request)
    {
        return $this->customerService->getPaginated(
            $request->getUrlParam('pageNo', 1),
            $request->getUrlParam('pageSize', 10),
            $request->getUrlParam('orderBy', 'id'),
            $request->getUrlParam('orderDir', 'desc'),
        );
    }

    public function get(Request $request, int $customerId)
    {
        return $this->customerService->getById($customerId);
    }
    public function create(Request $request)
    {
        $data = Validate::perform([
            'first_name' => ['required', 'min:2', 'max:50'],
            'last_name' => ['required', 'min:2', 'max:50'],
            'email' => ['required', 'email', 'unique:customers'],
            'gender' => ['required', 'in:' . implode(',', array_map(fn($v) => $v->value, \Gender::cases()))],
            'country' => ['required', 'min_length:2', 'max_length:2']
        ], $request->values());
        return $this->customerService->create($data);
    }

    public function update(Request $request, int $customerId)
    {
        $customer = $this->customerService->getById($customerId);
        if (!$customer) {
            Response::setStatusCode(404);
            return ['message' => "Customer not found"];
        }
        $data = Validate::perform([
            'first_name' => ['required', 'min:2', 'max:50'],
            'last_name' => ['required', 'min:2', 'max:50'],
            'email' => ['required', 'email', 'unique:customers,email,' . $customerId],
            'gender' => ['required', 'in:' . implode(',', array_map(fn($v) => $v->value, \Gender::cases()))],
            'country' => ['required', 'min_length:2', 'max_length:2']
        ], $request->values());
        return $this->customerService->update($customer, $data);
    }

    public function deposit(Request $request, int $customerId)
    {
        $customer = $this->customerService->getById($customerId);
        if (!$customer) {
            Response::setStatusCode(404);
            return ['message' => "Customer not found"];
        }
        $data = Validate::perform(['value' => ['required', 'numeric']], $request->values());
        return $this->customerService->deposit($customer, $data['value']);
    }

    public function withdraw(Request $request, int $customerId)
    {
        $customer = $this->customerService->getById($customerId);
        if (!$customer) {
            Response::setStatusCode(404);
            return ['message' => "Customer not found"];
        }
        $data = Validate::perform(['value' => ['required', 'numeric']], $request->values());
        try {
            return $this->customerService->withdraw($customer, $data['value']);
        } catch (\InvalidArgumentException $exception) {
            if ($exception->getMessage() == 'value_greater_than_real_balance') {
                throw new \exceptions\ValidationException([
                    'value' => [
                        'The value to withdraw is greater that the customer balance'
                    ]
                ]);
            } else {
                throw new \InvalidArgumentException($exception->getMessage(), $exception->getCode(), $exception->getPrevious());
            }
        }
    }
}