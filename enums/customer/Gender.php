<?php

enum Gender: string
{
    case MALE = 'M';
    case FEMALE = 'F';
}
