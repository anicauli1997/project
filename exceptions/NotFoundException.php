<?php

namespace exceptions;

class NotFoundException extends SystemException
{
    public function __construct(string $message = "", int $code = 0, ?\Throwable $previous = null)
    {
        if (!$message) {
            $message = 'Not found';
        }
        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode(): int
    {
        return 404;
    }

    public function getResponse(): array
    {
        return [
            'message' => $this->getMessage()
        ];
    }
}