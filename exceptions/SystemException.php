<?php

namespace exceptions;

class SystemException extends \Exception
{
    public function getResponse(): array
    {
        return [];
    }

    public function getStatusCode(): int
    {
        return 500;
    }
}