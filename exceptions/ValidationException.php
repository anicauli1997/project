<?php
namespace exceptions;
class ValidationException extends SystemException
{
    protected mixed $errors;
    public function __construct($errors = [])
    {
        parent::__construct('The given data is invalid', $this->getStatusCode());
        $this->errors = $errors;
    }

    /**
     * @return array|mixed
     */
    public function getErrors(): mixed
    {
        return $this->errors;
    }

    public function getStatusCode(): int
    {
        return 422;
    }

    public function getResponse(): array
    {
        return [
            'message' => $this->getMessage(),
            'errors' => $this->getErrors()
        ];
    }
}