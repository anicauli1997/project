<?php
require ('./exceptions/SystemException.php');
require ('./exceptions/ValidationException.php');
require ('./exceptions/NotFoundException.php');
require ('./models/BaseModel.php');
require ('./models/Customer.php');
require ('./models/Deposit.php');
require ('./models/Withdraw.php');
require ('./models/CountryDayReport.php');
require ("./utils/Rules.php");
require ("./utils/Validate.php");
require ('./utils/DB.php');
require ('./utils/Request.php');
require ('./utils/Response.php');
require ('./utils/ControllerDispatcher.php');
require ('./services/CustomerService.php');
require ('./services/CountryDayReportService.php');
require ('./controllers/CustomerController.php');
require ('./controllers/CountryDayReportController.php');
require ('./enums/customer/Gender.php');

use exceptions\SystemException;
use utils\ControllerDispatcher;
use utils\Request;
use \controllers\CustomerController;
use \controllers\CountryDayReportController;
setUpDatabase();
setUpResponse();
try {
    global $REQUEST;
    $REQUEST = new Request();
    $action = $REQUEST->getAction();
    if (!$action) {
        echo '<h1>Welcome</h1>';
        die();
    }
    if (str_starts_with($action, 'customers')) {
        deletePathPart($action, 'customers');
        if (str_starts_with($action, 'deposit')) {
            deletePathPart( $action, 'deposit');
            if ($action == '') {
                ControllerDispatcher::dispatch(CustomerController::class, 'deposit',
                    $REQUEST->getUrlParam('customerId', 0));
            }
        } else if (str_starts_with($action, 'withdraw')) {
            deletePathPart( $action, 'withdraw');
            if ($action == '') {
                ControllerDispatcher::dispatch(CustomerController::class, 'withdraw',
                    $REQUEST->getUrlParam('customerId', 0));
            }
        } else if ($action == '') {
            if ($REQUEST->getUrlParam('customerId', 0)) {
                ControllerDispatcher::dispatch(CustomerController::class, 'get',
                    $REQUEST->getUrlParam('customerId', 0));
            } else {
                ControllerDispatcher::dispatch(CustomerController::class, 'getAll');
            }

        } else if (str_starts_with($action, 'create')) {
            deletePathPart( $action, 'create');
            ControllerDispatcher::dispatch(CustomerController::class, 'create');
        } else if (str_starts_with($action, 'update')) {
            deletePathPart( $action, 'update');
            ControllerDispatcher::dispatch(
                CustomerController::class, 'update', $REQUEST->getUrlParam('customerId', 0));
        }
    } else if (str_starts_with($action, 'country-day-reports')) {
        deletePathPart( $action, 'country-day-reports');
        if ($action == '') {
            ControllerDispatcher::dispatch(
                CountryDayReportController::class, 'getAll');
        } else if ($action == 'direct-calculation') {
            deletePathPart( $action, 'direct-calculation');
            ControllerDispatcher::dispatch(
                CountryDayReportController::class, 'getDirectCalculation');
        }
    }
    if ($action) {
        throw new \exceptions\NotFoundException();
    }
} catch (SystemException $exception) {
    header(header: 'Content-Type: application/json', response_code: $exception->getStatusCode());
    echo json_encode($exception->getResponse());
} catch (Exception $exception) {
    header(header: 'Content-Type: application/json', response_code: 500);
    echo json_encode([
        'message' => $exception->getMessage(),
        'trace' => $exception->getTraceAsString()
    ]);
}

function deletePathPart(string &$path, string $toDelete): void
{
    $path = str_replace($toDelete . '/', '', $path);
    $path = str_replace($toDelete, '', $path);
}

function setUpDatabase(): void
{
    $host = 'mysql';
    $database = 'database';
    $username = 'root';
    $password = 'password';
    try {
       global $DATABASE__CONNECTION;
        $DATABASE__CONNECTION = new PDO("mysql:host={$host};dbname={$database}", $username, $password);
    } catch (PDOException $e) {
        header(header: 'Content-Type: application/json', response_code: 500);
        echo json_encode(['message' => $e->getMessage(),'trace' => $e->getTraceAsString()]);
        die();
    }
}

function setUpResponse(): void
{
    global $RESPONSE;
    $RESPONSE = [
        'header' => 'Content-Type:application/json',
        'response_code' => 200
    ];
}