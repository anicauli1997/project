<?php

namespace models;

use utils\DB;

abstract class BaseModel
{
    public ?int $id;
    public ?string $created_at;
    public ?string $updated_at;

    protected string $DATABASE__table;
    protected string $DATABASE__primaryKey = 'id';
    protected string $DATABASE__createdField = 'created_at';
    protected string $DATABASE__updatedField = 'updated_at';

    protected array $DATABASE__attributes = [];

    public function __construct()
    {
        foreach ($this->getModelFields() as $field) {
            if (!isset($this->{$field})) {
                $this->{$field} = null;
            }
        }
    }

    public function getDATABASE__PrimaryKey(): string
    {
        if (!$this->DATABASE__primaryKey) {
            throw new \Exception('The primary key is not specified');
        }

        return $this->DATABASE__primaryKey;
    }

    public function getDATABASE__Table(): string
    {
        if (!$this->DATABASE__table) {
            throw new \Exception('The table name is not specified');
        }

        return $this->DATABASE__table;
    }

    public function delete(): bool
    {
        $primaryKey = $this->getDATABASE__PrimaryKey();
        $table = $this->getDATABASE__Table();
        $query = "delete from {$table} where {$primaryKey}=:primaryKey";
        DB::delete($query, ['primaryKey' => $this->{$primaryKey}]);
        return true;
    }

    public function save(): void
    {
        $primaryKey = $this->getDATABASE__PrimaryKey();
        $table = $this->getDATABASE__Table();
        $now = date('Y-m-d H:i:s');
        if (!$this->{$primaryKey}) {
            $this->create($primaryKey, $table, $now);
        } else {
            $this->update($primaryKey, $table, $now);
        }
    }

    private function create($primaryKey, $table, $now): void
    {
        $this->{$this->DATABASE__createdField} = $now;
        $this->{$this->DATABASE__updatedField} = $now;
        $attributes = $this->getModelAttributes();
        unset($attributes[$primaryKey]);
        $fields = array_keys($attributes);
        $fieldsStringify = implode(', ', $fields);
        $fieldsBStringify = implode(', ', array_map(fn($f) => ":{$f}", $fields));
        $query = "insert into {$table} ({$fieldsStringify}) values ({$fieldsBStringify})";
        DB::create($query, $attributes);
        $this->{$primaryKey} = DB::lastInsertedId();
        $this->setDATABASE__attributes($this->getModelAttributes());
    }

    private function update($primaryKey, $table, $now): void
    {
        $attributes = $this->getModelAttributes();
        unset($attributes[$primaryKey]);
        $fieldsToUpdate = [];
        $fieldsToUpdateString = [];
        foreach ($attributes as $field => $value) {
            if ($value != $this->DATABASE__attributes[$field]) {
                $fieldsToUpdate[$field] = $value;
                $fieldsToUpdateString[] = "{$field}=:{$field}";
            }
        }
        if (count($fieldsToUpdate)) {
            $this->{$this->DATABASE__updatedField} = $now;
            $attributes[$this->DATABASE__updatedField] = $now;
            $fieldsToUpdate[$this->DATABASE__updatedField] = $now;
            $fieldsToUpdateString[] = "{$this->DATABASE__updatedField}=:{$this->DATABASE__updatedField}";
            $fieldsToUpdateString = implode(', ', $fieldsToUpdateString);
            $query = "update {$table} set {$fieldsToUpdateString} where {$primaryKey}=:DATABASEPrimaryKey";
            $fieldsToUpdate['DATABASEPrimaryKey'] = $this->{$primaryKey};
            DB::update($query, $fieldsToUpdate);
            $this->setDATABASE__attributes($this->getModelAttributes());
        }
    }

    private function getModelFields(): array
    {
        $calledClass = get_called_class();
        $classReflection = new \ReflectionClass($calledClass);
        $publicProperties = [];
        foreach ($classReflection->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $propertyName = $property->getName();
            $publicProperties[] = $propertyName;
        }
        return $publicProperties;
    }

    public function getModelAttributes(): array
    {
        $attributes = [];
        foreach ($this->getModelFields() as $field) {
            $attributes[$field] = $this->{$field};
        }
        return $attributes;
    }

    public function setDATABASE__attributes(array $attributes): void
    {
        $this->DATABASE__attributes = $attributes;
    }

    public static function get(array $fields = ['*'], string $conditions = '', array $bindings = [],
                               string $orders = '', bool $forUpdate = false): array
    {
        $class = get_called_class();
        $model = new $class();
        $table = $model->getDATABASE__Table();

        $fieldsStringify = implode(',', $fields);
        $query = "select {$fieldsStringify} from {$table}";
        if ($conditions) {
            $conditions = trim($conditions);
            $query .= " where {$conditions}";
        }
        if ($orders) {
            $orders = trim($orders);
            $query .= " order by {$orders}";
        }
        if ($forUpdate) {
            $query .= ' for update';
        }
        $results = DB::fetchAll($query, $bindings);
        $models = [];
        foreach ($results as $result) {
            $model = new $class();
            foreach ($result as $key => $value) {
                $model->{$key} = $value;
            }
            $model->setDATABASE__attributes($model->getModelAttributes());
            $models[] = $model;
        }
        return $models;
    }

    public static function getFirst(array $fields = ['*'], string $conditions = '', array $bindings = [],
                                    string $orders = '', bool $forUpdate = false): BaseModel|null
    {
        $class = get_called_class();
        $model = new $class();
        $table = $model->getDATABASE__Table();

        $fieldsStringify = implode(',', $fields);
        $query = "select {$fieldsStringify} from {$table}";
        if ($conditions) {
            $conditions = trim($conditions);
            $query .= " where {$conditions}";
        }
        if ($orders) {
            $orders = trim($orders);
            $query .= " order by {$orders}";
        }
        $query .= " limit 1";
        if ($forUpdate) {
            $query .= ' for update';
        }
        $result = DB::fetchOne($query, $bindings);
        if (!$result) {
            return null;
        }
        $model = new $class();
        foreach ($result as $key => $value) {
            $model->{$key} = $value;
        }
        $model->setDATABASE__attributes($model->getModelAttributes());
        return $model;
    }

    public static function paginate (array $fields = ['*'], string $conditions = '', array $bindings = [], string $orders = '',
        int $page = 1, int $pageSize = 10, bool $forUpdate = false
    ) {
        $class = get_called_class();
        $model = new $class();
        $table = $model->getDATABASE__Table();

        $fieldsStringify = implode(',', $fields);
        $baseQuery = "";
        if ($conditions) {
            $conditions = trim($conditions);
            $baseQuery .= " where {$conditions}";
        }
        if ($orders) {
            $orders = trim($orders);
            $baseQuery .= " order by {$orders}";
        }
        $aggregateQuery = "select count(*) from {$table} " . $baseQuery;
        $query = "select {$fieldsStringify} from {$table} " . $baseQuery;
        $offset = ($page - 1) * $pageSize;
        $query .= " limit {$offset}, {$pageSize}";
        if ($forUpdate) {
            $query .= ' for update';
        }
        $count = DB::count($aggregateQuery, $bindings);
        $models = [];
        $pageResults = DB::fetchAll($query, $bindings);
        foreach ($pageResults as $result) {
            $model = new $class();
            foreach ($result as $key => $value) {
                $model->{$key} = $value;
            }
            $model->setDATABASE__attributes($model->getModelAttributes());
            $models[] = $model;
        }
        $totalPages = ceil($count/$pageSize);
        return [
            'current_page' => $page,
            'previous_page' => $page == 1 ? null : $page - 1,
            'next_page' => $page == $totalPages ? null : $page + 1,
            'total_pages' => $totalPages,
            'count' => $count,
            'results' => $models
        ];
    }

    public static function count(string $conditions = '', array $bindings = []): int
    {
        $class = get_called_class();
        $model = new $class();
        $table = $model->getDATABASE__Table();

        $query = "";
        if ($conditions) {
            $conditions = trim($conditions);
            $query .= " where {$conditions}";
        }
        $aggregateQuery = "select count(*) from {$table} " . $query;
        return DB::count($aggregateQuery, $bindings);
    }
}