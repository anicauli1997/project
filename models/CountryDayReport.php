<?php

namespace models;

class CountryDayReport extends BaseModel
{
    public ?string $date;
    public ?string $country;
    public ?int $unique_customers = 0;
    public ?int $no_of_deposits = 0;
    public ?float $total_deposit_amount = 0;
    public ?int $no_of_withdrawal = 0;
    public ?float $total_withdrawal_amount = 0;
    protected string $DATABASE__table = 'country_day_reports';
}