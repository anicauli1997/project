<?php

namespace models;

class Customer extends BaseModel
{
    public ?string $first_name;
    public ?string $last_name;
    public ?string $email;
    public ?string $country;
    public ?string $gender;
    public ?int $bonus_rate;
    public ?float $total_balance = 0;
    public ?float $real_balance = 0;
    public ?float $bonus_balance = 0;
    public ?float $total_deposit = 0;
    public ?float $real_deposit = 0;
    public ?float $bonus_deposit = 0;
    public ?float $real_withdraw = 0;
    protected string $DATABASE__table = 'customers';
}