<?php

namespace models;

class Deposit extends BaseModel
{
    public ?int $customer_id;
    public ?float $value;
    public ?float $bonus_value;
    public ?float $total_value;
    public ?int $bonus_rate;
    protected string $DATABASE__table = 'deposits';
}