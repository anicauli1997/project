<?php

namespace models;

class Withdraw extends BaseModel
{
    public ?int $customer_id;
    public ?float $value;
    protected string $DATABASE__table = 'withdraws';
}