CREATE TABLE `customers` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `first_name` varchar(100) NOT NULL,
     `last_name` varchar(100) NOT NULL,
     `email` varchar(150) NOT NULL,
     `gender` enum('M','F') NOT NULL,
     `country` varchar(2) NOT NULL,
     `bonus_rate` smallint(6) NOT NULL DEFAULT 0,
     `total_balance` decimal(10,2) NOT NULL DEFAULT 0.00,
     `real_balance` decimal(10,2) NOT NULL DEFAULT 0.00,
     `bonus_balance` decimal(10,2) NOT NULL DEFAULT 0.00,
     `total_deposit` decimal(10,2) NOT NULL DEFAULT 0.00,
     `real_deposit` decimal(10,2) NOT NULL DEFAULT 0.00,
     `bonus_deposit` decimal(10,2) NOT NULL DEFAULT 0.00,
     `real_withdraw` decimal(10,2) NOT NULL DEFAULT 0.00,
     `created_at` timestamp NULL DEFAULT NULL,
     `updated_at` timestamp NULL DEFAULT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `email` (`email`)
);


CREATE TABLE `deposits` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `customer_id` int(11) NOT NULL,
    `value` decimal(10,2) NOT NULL DEFAULT 0.00,
    `bonus_value` decimal(10,2) NOT NULL DEFAULT 0.00,
    `total_value` decimal(10,2) NOT NULL DEFAULT 0.00,
    `bonus_rate` int(11) NOT NULL DEFAULT 0,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `customer_id` (`customer_id`),
    CONSTRAINT `deposits_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
);

CREATE TABLE `withdraws` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `customer_id` int(11) NOT NULL,
 `value` decimal(10,2) NOT NULL DEFAULT 0.00,
 `created_at` timestamp NULL DEFAULT NULL,
 `updated_at` timestamp NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `customer_id` (`customer_id`),
 CONSTRAINT `withdraws_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
);

CREATE TABLE `country_day_reports` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `date` date NOT NULL,
   `country` varchar(2) NOT NULL,
   `unique_customers` int(11) NOT NULL DEFAULT 0,
   `no_of_deposits` int(11) NOT NULL DEFAULT 0,
   `total_deposit_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
   `no_of_withdrawal` int(11) NOT NULL DEFAULT 0,
   `total_withdrawal_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
   `created_at` timestamp NULL DEFAULT NULL,
   `updated_at` timestamp NULL DEFAULT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `date_country_unique` (`date`,`country`)
);