## Introduction and starting
This project (task) is build using pure PHP in an OOP methodology.

Version of PHP: 8.2

Database: MYSQL

### Start (Windows)
1. Download and install xampp 8.2 https://www.apachefriends.org/download.html
2. Go to the root directory where the xampp is installed(usually) and run xampp-control. Then start apache and mysql
3. Locale the project folder inside htdocs folder that is on root directory of xampp
4. Create a database, execute the sql scripts that are in `/mysql/project.sql` on this database 
and update the credentials inside `setUpDatabase` function in `index.php` file
in `index.php` file in the project folder
5. Then you can run the functionalities using `http://localhost/{project_folder}`

### Start application with Docker
1. Install Docker in your machine
2. Go to the project root directory, and execute: docker-compose up
3. Open the browser and navigate to http://localhost:8080.
If you see the "Welcome" message everything is done successfully :)

## Endpoints

### Get customers
| Method | Path                  | Description                    |
|--------|-----------------------|--------------------------------|
| GET    | `/?action=customers`  | Get the list of the customers  |

#### Paginate / order params
| Param    | Default | Description                   |
|----------|---------|-------------------------------|
| pageNo   | 1       | Page Number                   |
| pageSize | 10      | Page Size                     |
| orderBy  | id      | Column to order by            |
| orderDir | desc    | Order direction (asc or desc) |

### Get single customer
| Method | Path                               | Description           |
|--------|------------------------------------|-----------------------|
| GET    | `/?action=customers&customerId=1`  | Get a single customer |

### Create customer
| Method | Path                        | Description       |
|--------|-----------------------------|-------------------|
| POST   | `/?action=customers/create` | Create a customer |

#### Body
| Param       | Required | Type                |
|-------------|----------|---------------------|
| first_name  | Yes      | string              |
| last_name   | Yes      | string              |
| email       | Yes      | string              |
| gender      | Yes      | string ('M' or 'F') |
| country     | Yes      | string (2 chars)    |

### Update customer
| Method | Path                                      | Description       |
|--------|-------------------------------------------|-------------------|
| POST   | `/?action=customers/update&customerId=1 ` | Update a customer |

#### Body
| Param       | Required | Type                |
|-------------|----------|---------------------|
| first_name  | Yes      | string              |
| last_name   | Yes      | string              |
| email       | Yes      | string              |
| gender      | Yes      | string ('M' or 'F') |
| country     | Yes      | string (2 chars)    |

### Deposit
| Method | Path                                       | Description    |
|--------|--------------------------------------------|----------------|
| POST   | `/?action=customers/deposit&customerId=1 ` | Make a deposit |

#### Body
| Param     | Required | Type   |
|-----------|----------|--------|
| value     | Yes      | number |

### Withdraw
| Method | Path                                        | Description     |
|--------|---------------------------------------------|-----------------|
| POST   | `/?action=customers/withdraw&customerId=1 ` | Make a withdraw |

#### Body
| Param     | Required | Type   |
|-----------|----------|--------|
| value     | Yes      | number |

### Get the report
| Method | Path                             | Description     |
|--------|----------------------------------|-----------------|
| GET    | `/?action=country-day-reports  ` | Make a withdraw |

You can put a date as a query string param using key `from_date` to specify the start date

Example: &from_date=2023-02-05

The default is the date seven days ago

Here you can use also the pagination and order params used in the getting customer list endpoint 

### Get the report (direct calculated)
| Method | Path                                              | Description     |
|--------|---------------------------------------------------|-----------------|
| GET    | `/?action=country-day-reports/direct-calculation` | Make a withdraw |

You can use the `from_date` here as well

**Important note**

I have built two ways how to generate the reports

The first one (`Get the report`) gets the data from the `country_day_reports` table
that are collected while deposits/withdraws are made.

The second way is generated getting data from `deposits` table and 'withdraws' table and using code logic to process
the right data.


## Workflow

Everything starts on `index.php` file.

There are included everything needed, is set up the database and other initial stuff. 

It is used as an engine to trigger all the functionalities.

To know what to execute, is used `action` query string param, pretty much as a routing path
If the action value find any combination, a controller function is executed using `ControllerDispatcher`, otherwise,
a NotFoundException is throws
In the controller is always passed the global Request instance, that is an object that is used to 
manage request parameters (query-strings, body etc.)

Every controller functions, use the service layer to perform the business actions.
Just for having a good structure of the project.


## Main classes

### BaseModel
`BaseModel` class in a base class that is used to be extended from the other classes that are linked with a database table.

There are configured some methods to interact with database, in order to use some functions instead of raw queries
and manage the data using objects. There are 4 model that we have here that are related with 4 database tables

### DB
`DB` is a class that contains methods to interact with database.

### Validate
`Validate` is used to validate the data based on some rules that there are implemented
The method that triggers the validation is `perform` method.
It accepts the rules and the data to be validated. If the validation is passed, it returns the data that are validated,
otherwise it throws a `ValidationException`.

