<?php

namespace services;

use models\CountryDayReport;
use utils\DB;

class CountryDayReportService
{
    public function getPaginated(
        int $pageNo = 1, int $pageSize = 10, string $orderBy = 'id', string $orderDir = 'desc', string $fromDate = null): array
    {
        if (!$fromDate) {
            $date = new \DateTime();
            $date->modify('-7 days');
            $date = $date->format('Y-m-d');
        } else {
            $date = $fromDate;
        }
        $condition = 'DATE(created_at) >= :date';
        return CountryDayReport::paginate(conditions: $condition, bindings: ['date' => $date],
            orders: "{$orderBy} {$orderDir}", page: $pageNo, pageSize: $pageSize);
    }

    public function getDirectCalculation(string $fromDate = null): array
    {
        if (!$fromDate) {
            $date = new \DateTime();
            $date->modify('-7 days');
            $date = $date->format('Y-m-d');
        } else {
            $date = $fromDate;
        }
        $results = [];
        $customersInDate = [];
        $deposits = DB::fetchAll($this->getMovementQuery('deposits'), ['date' => $date]);
        foreach ($deposits as $deposit) {
            $date = explode(' ', $deposit['created_at'])[0];
            $key = $this->generateRecordsKey($date, $deposit['country']);
            if (!isset($results[$key])) {
                $results[$key] = $this->getEmptyRecord($date, $deposit['country']);
            }
            if ($this->checkUniqueCustomer($customersInDate, $date, $deposit['customer_id'])) {
                $results[$key]['unique_customers']++;
            }
            $results[$key]['no_of_deposits']++;
            $results[$key]['total_deposit_amount']+= $deposit['total_value'];
        }
        $withdraws = DB::fetchAll($this->getMovementQuery('withdraws'), ['date' => $date]);
        foreach ($withdraws as $withdraw) {
            $date = explode(' ', $withdraw['created_at'])[0];
            $key = $this->generateRecordsKey($date, $withdraw['country']);
            if (!isset($results[$key])) {
                $results[$key] = $this->getEmptyRecord($date, $withdraw['country']);
            }
            if ($this->checkUniqueCustomer($customersInDate, $date, $withdraw['customer_id'])) {
                $results[$key]['unique_customers']++;
            }
            $results[$key]['no_of_withdrawal']++;
            $results[$key]['total_withdrawal_amount']+= $withdraw['value'];
        }
        return array_values($results);
    }

    public function checkUniqueCustomer(array &$customersInDate,string $date, int $customerId): bool
    {
        $isUnique = false;
        if (!isset($customersInDate[$date])) {
            $customersInDate[$date] = [$customerId];
            $isUnique = true;
        } else if (!in_array($customerId, $customersInDate[$date])) {
            $customersInDate[$date][] = $customerId;
            $isUnique = true;
        }
        return $isUnique;
    }

    private function generateRecordsKey(string $date, string $country): string
    {
        return $date . '-' . $country;
    }

    private function getEmptyRecord(string $date, string $country): array
    {
        return  [
            'date' => $date,
            'country' => $country,
            'unique_customers' => 0,
            'no_of_deposits' => 0,
            'total_deposit_amount' => 0,
            'no_of_withdrawal' => 0,
            'total_withdrawal_amount' => 0,
        ];
    }

    private function getMovementQuery(string $table): string
    {
        $query = "select {$table}.*, customers.country from {$table} ";
        $query .= " left join customers on customer_id = customers.id where DATE({$table}.created_at) >= :date";
        return $query;
    }
}