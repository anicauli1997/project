<?php

namespace services;

use models\CountryDayReport;
use models\Customer;
use models\Deposit;
use models\Withdraw;
use utils\DB;

class CustomerService
{
    public function getPaginated(
        int $pageNo = 1, int $pageSize = 10, string $orderBy = 'id', string $orderDir = 'desc'): array
    {
        return Customer::paginate(page: $pageNo, pageSize: $pageSize, orders: "{$orderBy} {$orderDir}");
    }

    public function getById(int $customerId, bool $forUpdate = false): Customer|null
    {
        return Customer::getFirst(conditions: "id = :id", bindings: ['id' => $customerId], forUpdate: $forUpdate);
    }
    public function create(array $data): Customer
    {
        $customer = new Customer();
        $customer->first_name = $data['first_name'];
        $customer->last_name = $data['last_name'];
        $customer->email = $data['email'];
        $customer->gender = $data['gender'];
        $customer->country = $data['country'];
        $customer->bonus_rate = rand(5, 20);
        $customer->save();
        return $customer;
    }

    public function update(Customer $customer, array $data): Customer
    {
        foreach ($data as $field => $value) {
            $customer->{$field} = $value;
        }
        $customer->save();
        return $customer;
    }

    public function deposit(Customer $customer, float|int $realDeposit): Deposit
    {
        return DB::transactional(function () use ($customer, $realDeposit) {
            $customer = $this->getById($customer->id, true);
            $count = Deposit::count(conditions: "customer_id=:customerId", bindings: [
                'customerId' => $customer->id
            ]);
            if ($count != 0 && ($count + 1) % 3 == 0) {
                $bonusDeposit = $customer->bonus_rate / 100 * $realDeposit;
                $totalDeposit = $bonusDeposit + $realDeposit;
            } else {
                $bonusDeposit = 0;
                $totalDeposit = $realDeposit;
            }

            $deposit = new Deposit();
            $deposit->value = $realDeposit;
            $deposit->customer_id = $customer->id;
            $deposit->bonus_value = $bonusDeposit;
            $deposit->total_value = $totalDeposit;
            $deposit->bonus_rate = $customer->bonus_rate;
            $deposit->save();

            $customer->real_deposit = $customer->real_deposit + $realDeposit;
            $customer->bonus_deposit = $customer->bonus_deposit + $bonusDeposit;
            $customer->total_deposit = $customer->total_deposit + $totalDeposit;

            $customer->real_balance = $customer->real_balance + $realDeposit;
            $customer->bonus_balance = $customer->bonus_balance + $bonusDeposit;
            $customer->total_balance = $customer->total_balance + $totalDeposit;

            $customer->save();
            $date = date('Y-m-d');
            $countryDayReport = $this->checkAndGetCountryDayReport($customer, $date);
            if (!$countryDayReport->id) {
                $countryDayReport->unique_customers = 1;
                $countryDayReport->no_of_deposits = 1;
                $countryDayReport->total_deposit_amount = $deposit->total_value;
                $countryDayReport->save();
            } else {
                $isUnique = true;
                $depositCount = Deposit::count(
                    conditions: 'DATE(created_at)=:date and customer_id=:customer_id',
                    bindings: ['date' => $date, 'customer_id' => $customer->id]
                );
                if ($depositCount > 1) {
                    $isUnique = false;
                } else {
                    $withDrawlCount = Withdraw::count(
                        conditions: 'DATE(created_at)=:date and customer_id=:customer_id',
                        bindings: ['date' => $date, 'customer_id' => $customer->id]
                    );
                    if ($withDrawlCount > 0) {
                        $isUnique = false;
                    }
                }
                if ($isUnique) {
                    $countryDayReport->unique_customers = $countryDayReport->unique_customers + 1;
                }
                $countryDayReport->no_of_deposits = $countryDayReport->no_of_deposits + 1;
                $countryDayReport->total_deposit_amount = $countryDayReport->total_deposit_amount + $deposit->total_value;
            }
            $countryDayReport->save();
            return $deposit;
        });
    }

    public function withdraw(Customer $customer, float|int $value): Withdraw
    {
        return DB::transactional(function () use ($customer, $value) {
            $customer = $this->getById($customer->id, true);
            if ($value > $customer->real_balance) {
                throw new \InvalidArgumentException('value_greater_than_real_balance');
            }
            $withdraw = new Withdraw();
            $withdraw->customer_id = $customer->id;
            $withdraw->value = $value;
            $withdraw->save();

            $customer->real_balance = $customer->real_balance - $value;
            $customer->total_balance = $customer->total_balance - $value;
            $customer->real_withdraw = $customer->real_withdraw + $value;
            $customer->save();
            $date = date('Y-m-d');
            $countryDayReport = $this->checkAndGetCountryDayReport($customer, $date);
            if (!$countryDayReport->id) {
                $countryDayReport->unique_customers = 1;
                $countryDayReport->no_of_withdrawal = 1;
                $countryDayReport->total_withdrawal_amount = $withdraw->value;
                $countryDayReport->save();
            } else {
                $isUnique = true;
                $withDrawlCount = Withdraw::count(
                    conditions: 'DATE(created_at)=:date and customer_id=:customer_id',
                    bindings: ['date' => $date, 'customer_id' => $customer->id]
                );
                if ($withDrawlCount > 1) {
                    $isUnique = false;
                } else {
                    $depositCount = Deposit::count(
                        conditions: 'DATE(created_at)=:date and customer_id=:customer_id',
                        bindings: ['date' => $date, 'customer_id' => $customer->id]
                    );
                    if ($depositCount > 0) {
                        $isUnique = false;
                    }
                }
                if ($isUnique) {
                    $countryDayReport->unique_customers = $countryDayReport->unique_customers + 1;
                }
                $countryDayReport->no_of_withdrawal = $countryDayReport->no_of_withdrawal + 1;
                $countryDayReport->total_withdrawal_amount = $countryDayReport->total_withdrawal_amount + $withdraw->value;
            }
            $countryDayReport->save();
            return $withdraw;
        });
    }

    private function checkAndGetCountryDayReport(Customer $customer, string $date): CountryDayReport
    {
        $countryDayReport = CountryDayReport::getFirst(conditions: 'date=:date and country=:country', bindings: [
            'date' => $date,
            'country' => $customer->country
        ], forUpdate: true);
        if (!$countryDayReport) {
            $countryDayReport = new CountryDayReport();
            $countryDayReport->date = $date;
            $countryDayReport->country = $customer->country;
        }
        return $countryDayReport;
    }
}