<?php

namespace utils;

abstract class ControllerDispatcher
{
    public static function dispatch(string $controller, string $method, ...$args): void
    {
        global $REQUEST;
        $result = (new $controller())->{$method}($REQUEST, ...$args);
        global $RESPONSE;
        $header = $RESPONSE['header'];
        $responseCode = $RESPONSE['response_code'];
        header(header: $header, response_code: $responseCode);
        if (str_contains($header,'application/json')) {
            try {
                echo json_encode($result);
            } catch (\Exception $exception) {
                echo $result;
            }
        } else {
            echo $result;
        }
    }
}