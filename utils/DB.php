<?php

namespace utils;

class DB
{
    private static function mapRecords(array $records, string $classToMap): array
    {
        $instances = [];
        foreach ($records as $record) {
            $instance = new $classToMap();
            foreach ($record as $key => $value) {
                $instance->{$key} = $value;
            }
            $instances[] = $instance;
        }
        return $instances;
    }
    public static function fetchAll($query, $data = [], string $classToMap = null): array
    {
        global $DATABASE__CONNECTION;
        $statement = $DATABASE__CONNECTION->prepare($query);
        $statement->execute($data);
        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $classToMap ? self::mapRecords($results, $classToMap) : $results;
    }

    public static function fetchOne($query, $data = [], string $classToMap = null): array|null
    {
        global $DATABASE__CONNECTION;
        $statement = $DATABASE__CONNECTION->prepare($query);
        $statement->execute($data);
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }
        return $classToMap ? self::mapRecords($result, $classToMap)[0] : $result;
    }

    public static function create($query, $data = []): void
    {
        global $DATABASE__CONNECTION;
        $statement = $DATABASE__CONNECTION->prepare($query);
        $statement->execute($data);
    }

    public static function update($query, $data = []): void
    {
        global $DATABASE__CONNECTION;
        $statement = $DATABASE__CONNECTION->prepare($query);
        $statement->execute($data);
    }

    public static function lastInsertedId(): mixed
    {
        global $DATABASE__CONNECTION;
        return $DATABASE__CONNECTION->lastInsertId();
    }

    public static function delete($query, $data = []): void
    {
        global $DATABASE__CONNECTION;
        $statement = $DATABASE__CONNECTION->prepare($query);
        $statement->execute($data);
    }

    public static function count($query, $data = []): int
    {
        global $DATABASE__CONNECTION;
        $statement = $DATABASE__CONNECTION->prepare($query);
        $statement->execute($data);
        return $statement->fetchColumn();
    }

    public static function transactional(callable $callback): mixed
    {
        global $DATABASE__CONNECTION;
        try {
            $DATABASE__CONNECTION->beginTransaction();
            $results = $callback();
            $DATABASE__CONNECTION->commit();
            return $results;
        } catch (\PDOException $exception) {
            $DATABASE__CONNECTION->rollBack();
            throw new \PDOException($exception->getMessage(), $exception->getCode(), $exception->getPrevious());
        }
    }
}