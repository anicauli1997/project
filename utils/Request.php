<?php

namespace utils;

#[\AllowDynamicProperties]
class Request
{
    public function __construct()
    {
        $getValues = $_GET;
        foreach ($getValues as $key => $value) {
            $this->{$key} = $value;
        }
        $postValues = $_POST;
        foreach ($postValues as $key => $value) {
            $this->{$key} = $value;
        }
        $bodyValues = $this->getBody();
        foreach ($bodyValues as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function __get(string $name)
    {
        if (!property_exists($this, $name)) {
            return null;
        }
        return $this->{$name};
    }

    public function getBody(): array
    {
        try {
            if (!file_get_contents('php://input')) {
                return [];
            }
            return json_decode(file_get_contents('php://input'), true);
        } catch (\Exception $exception){
            return [];
        }
    }

    public function values(): array
    {
        $attributes = [];
        foreach ($this as $field => $value) {
            $attributes[$field] = $value;
        }
        return $attributes;
    }

    public function getAction(): string
    {
        return $_GET['action'] ?? '';
    }

    public function getUrlParam(string $name, mixed $default = null): string|null
    {
        return $this->getUrlParams()[$name] ?? $default;
    }

    public function getUrlParams(): array
    {
        $params = [];
        foreach ($_GET as $key => $value) {
            if ($key == 'action') {
                continue;
            }
            $params[$key] = $value;
        }
        return $params;
    }
}