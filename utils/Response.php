<?php

namespace utils;

class Response
{
    public static function setStatusCode(int $statusCode): void
    {
        global $RESPONSE;
        $RESPONSE['response_code'] = $statusCode;
    }
}