<?php

namespace utils;

enum Rules: string
{
    case WHEN_PRESENT = 'when_present';
    case REQUIRED = 'required';
    case UNIQUE = 'unique';
    case NUMERIC = 'numeric';
    case NULLABLE = 'nullable';
    case MIN_LENGTH = 'min_length';
    case MAX_LENGTH = 'max_length';
    case MAX = 'max';
    case MIN = 'min';
    case EMAIL = 'email';
    case IN = 'in';
    case DATE = 'date';
}
