<?php
namespace utils;
use exceptions\ValidationException;

class Validate
{
    private array $failures = [];
    private array $rules;
    private array $data;

    public array $validatedData = [];
    public function __construct(array $rules, array $data)
    {
        $this->rules = $rules;
        $this->data = $data;
    }

    public static function perform(array $rules, array $data): array
    {
        $instance = new self($rules, $data);
        $instance->validate();
        $failures = $instance->getFailures();
        if (count($failures)) {
            throw new ValidationException($failures);
        }
        return $instance->validatedData;
    }

    public function validate(): void
    {
        foreach ($this->rules as $field => $rules) {
            if (!is_array($rules)) {
                $rules = [$rules];
            }
            if (in_array(Rules::WHEN_PRESENT, $rules) && !array_key_exists($field, $this->data)) {
                continue;
            }
            $isValid = true;
            foreach ($rules as $rule) {
                if ($rule == Rules::WHEN_PRESENT) {
                    continue;
                }
                $passed = true;
                $split = explode(':', $rule);
                $rule = $split[0];
                $context = count($split) > 1 ? $split[1] : null;
                $value = array_key_exists($field, $this->data) ? $this->data[$field] : null;
                switch ($rule) {
                    case Rules::REQUIRED->value:
                        $passed = $this->checkRequired($field, $value, $rules);
                        break;
                    case Rules::UNIQUE->value:
                        $passed = $this->checkUnique($field, $value, $context);
                        break;
                    case Rules::NUMERIC->value:
                        $passed = $this->checkNumeric($field, $value);
                        break;
                    case Rules::MIN->value:
                        $passed = $this->checkMin($field, $value, $context);
                        break;
                    case Rules::MAX->value:
                        $passed = $this->checkMax($field, $value, $context);
                        break;
                    case Rules::MIN_LENGTH->value:
                        $passed = $this->checkMinLength($field, $value, $context);
                        break;
                    case Rules::MAX_LENGTH->value:
                        $passed = $this->checkMaxLength($field, $value, $context);
                        break;
                    case Rules::EMAIL->value:
                        $passed = $this->checkEmail($field, $value);
                        break;
                    case Rules::IN->value:
                        $passed = $this->checkIn($field, $value, $context);
                        break;
                    case Rules::DATE->value:
                        $passed = $this->checkDate($field, $value);
                        break;
                }
                if (!$passed) {
                    $isValid = false;
                }
            }
            if ($isValid && isset($this->data[$field])) {
                $this->validatedData[$field] = $this->data[$field];
            }
        }
    }

    public function getFailures(): array
    {
        return $this->failures;
    }

    private function checkRequired($field, $value, array $rules): bool
    {
        if (!in_array(Rules::NULLABLE->value, $rules) && !$this->isFilled($value)) {
            $this->addToFailures($field, Rules::REQUIRED->value);
            return false;
        }
        return true;
    }

    private function checkUnique($field, $value, $context): bool
    {
        if ($this->isFilled($value)) {
            $split = explode(',', $context);
            $table = $split[0];
            $column = $split[1] ?? $field;
            $excludeValue = $split[2] ?? null;
            $excludeField = $split[3] ?? 'id';
            $bindings = ['column_value' => $value];
            if ($excludeValue) {
                $query = "select * from {$table} where {$column}=:column_value and {$excludeField}!=:exclude_value limit 1";
                $bindings['exclude_value'] = $excludeValue;
            } else {
                $query = "select * from {$table} where {$column}=:column_value limit 1";
            }
            $result = DB::fetchOne($query, $bindings);
            if ($result) {
                $this->addToFailures($field, Rules::UNIQUE->value);
                return false;
            }
        }
        return true;
    }


    private function checkNumeric($field, $value): bool
    {
        if ($this->isFilled($value)) {
            if (!is_numeric($value)) {
                $this->addToFailures($field, Rules::NUMERIC->value);
                return false;
            }
        }
        return true;
    }

    private function checkMin($field, $value, $context): bool
    {
        if ($this->isFilled($value) && is_numeric($value)) {
            if ($value < $context) {
                $this->addToFailures($field, Rules::MIN->value, ['value' => $context]);
                return false;
            }
        }
        return true;
    }

    private function checkMax($field, $value, $context): bool
    {
        if ($this->isFilled($value) && is_numeric($value)) {
            if ($value > $context) {
                $this->addToFailures($field, Rules::MAX->value, ['value' => $context]);
                return false;
            }
        }
        return true;
    }

    private function checkMinLength($field, $value, $context): bool
    {
        if ($this->isFilled($value)) {
            if (strlen($value) < $context) {
                $this->addToFailures($field, Rules::MIN_LENGTH->value, ['length' => $context]);
                return false;
            }
        }
        return true;
    }

    private function checkMaxLength($field, $value, $context): bool
    {
        if ($this->isFilled($value)) {
            if (strlen($value) > $context) {
                $this->addToFailures($field, Rules::MAX_LENGTH->value, ['length' => $context]);
                return false;
            }
        }
        return true;
    }

    private function checkEmail($field, $value): bool
    {
        if ($this->isFilled($value)) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->addToFailures($field, Rules::EMAIL->value);
                return false;
            }
        }
        return true;
    }

    private function checkIn($field, $value, $context): bool
    {
        if ($this->isFilled($value)) {
            $allowedValues = explode(',', $context);
            $allowedValues = array_map(fn($v) => trim($v), $allowedValues);
            if (!in_array($value, $allowedValues)) {
                $this->addToFailures($field, Rules::IN->value, [
                    'allowed_values' => implode(', ', $allowedValues)
                ]);
                return false;
            }
        }
        return true;
    }


    private function checkDate($field, $value): bool
    {
        if ($this->isFilled($value)) {
            if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $value)) {
                $this->addToFailures($field, Rules::DATE->value);
                return false;
            }
        }
        return true;
    }
    private function addToFailures($field, $rule, $other = [], string $message = null): void
    {
        if (!isset($this->failures[$field])) {
            $this->failures[$field] = [];
        }

        $this->failures[$field][] = $message ?: $this->buildMessage($field, $rule, $other);
    }

    private function buildMessage($field, $rule, $other = []): string
    {
        $message = str_replace(':field', $field, $this->getMessage($rule));
        foreach ($other as $toReplace => $replace) {
            $message = str_replace(':' . $toReplace, $replace, $message);
        }
        return $message;
    }

    private function getMessage($rule): string
    {
        return [
            Rules::REQUIRED->value => 'The :field is required.',
            Rules::UNIQUE->value => 'The :field in already existing.',
            Rules::NUMERIC->value => 'The :field should be a numeric value.',
            Rules::MIN_LENGTH->value => 'The :field length should be greater of equal to :length.',
            Rules::MAX_LENGTH->value => 'The :field length should be greater of equal to :length.',
            Rules::MIN->value => 'The :field value should be greater of equal with :value.',
            Rules::MAX->value => 'The :field length should be lower of equal to :value.',
            Rules::EMAIL->value => 'The :field should be a valid email address.',
            Rules::IN->value => 'The :field allow only the values: :allowed_values.',
            Rules::DATE->value => 'The :field should be a regular date format.',
        ][$rule];
    }

    private function isFilled($value): bool
    {
        return !(is_null($value) || trim($value) == '');
    }
}